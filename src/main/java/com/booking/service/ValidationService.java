package com.booking.service;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class ValidationService {
    // Buatlah function sesuai dengan kebutuhan
    public static void validateInput() {

    }

    public static String validateInputUpdateReservation(String question, String errorMessage, String regex) {
        Scanner input = new Scanner(System.in);
        String result;

        boolean isLooping = true;
        do {
            isLooping = true;
            System.out.print(question);
            result = input.nextLine();// Hendr4
            // validasi menggunakan matches
            if (result.matches(regex)) {
                isLooping = false;
            } else {
                System.out.println();
                System.out.println("=============================================");
                System.out.println(errorMessage);
                System.out.println("=============================================");
                System.out.println();
            }
        } while (isLooping);

        return result;
    }

    public static String validateCustomerId(String question, String errorMessage, List<Person> customers) {
        Scanner input = new Scanner(System.in);
        String result;

        boolean isLooping = true;
        do {
            isLooping = true;
            PrintService.showAllCustomer("All Customers", customers);
            System.out.print(question);
            result = input.nextLine();// Hendr4
            List<Customer> target = new ArrayList<Customer>();
            // validasi menggunakan matches
            for (Person customer : customers) {
                if (customer instanceof Customer) {
                    if (((Customer) customer).getId().equalsIgnoreCase(result)) {
                        target.add(((Customer) customer));
                    }
                }
            }

            if (target.isEmpty()) {
                System.out.println();
                System.out.println("=============================================");
                System.out.println(errorMessage);
                System.out.println("=============================================");
                System.out.println();
            } else {
                isLooping = false;
            }
        } while (isLooping);
        // input.close();

        return result;
    }

    public static String validateEmpId(String question, String errorMessage, List<Person> employees) {
        Scanner input = new Scanner(System.in);
        String result;

        boolean isLooping = true;
        do {
            isLooping = true;
            PrintService.showAllEmployee("All Employee", employees);
            System.out.print(question);
            result = input.nextLine();// Hendr4
            List<Employee> target = new ArrayList<Employee>();
            // validasi menggunakan matches
            for (Person employee : employees) {
                if (employee instanceof Employee) {
                    if (((Employee) employee).getId().equalsIgnoreCase(result)) {
                        target.add(((Employee) employee));
                    }
                }
            }

            if (target.isEmpty()) {
                System.out.println();
                System.out.println("=============================================");
                System.out.println(errorMessage);
                System.out.println("=============================================");
                System.out.println();
            } else {
                isLooping = false;
            }
        } while (isLooping);
        // input.close();

        return result;
    }

    public static String validateServiceId(String question, String errorMessage, List<Service> services) {
        Scanner input = new Scanner(System.in);
        String result;

        boolean isLooping = true;
        do {
            isLooping = true;
            PrintService.showAllService("All Service", services);
            System.out.print(question);
            result = input.nextLine();// Hendr4
            List<Service> target = new ArrayList<Service>();
            // validasi menggunakan matches
            for (Service service : services) {
                if (service.getServiceId().equalsIgnoreCase(result)) {
                    target.add(service);
                }
            }

            if (target.isEmpty()) {
                System.out.println();
                System.out.println("=============================================");
                System.out.println(errorMessage);
                System.out.println("=============================================");
                System.out.println();
            } else {
                isLooping = false;
            }
        } while (isLooping);
        // input.close();

        return result;
    }

    public static String validateReservationId(String question, String errorMessage, List<Reservation> reservations) {
        Scanner input = new Scanner(System.in);
        String result;
        boolean isLooping = true;

        do {
            isLooping = true;
            PrintService.showAllReservation("All Reservation", reservations);
            System.out.print(question);
            result = input.nextLine();// Hendr4
            List<Reservation> target = new ArrayList<Reservation>();
            // validasi menggunakan matches
            for (Reservation reservation : reservations) {
                if (reservation.getReservationId().equalsIgnoreCase(result) && reservation.getWorkstage()
                        .equalsIgnoreCase("In Process") || reservation.getWorkstage().equalsIgnoreCase("Waiting")) {
                    target.add(reservation);
                }
            }

            if (target.isEmpty()) {
                System.out.println();
                System.out.println("=============================================");
                System.out.println(errorMessage);
                System.out.println("=============================================");
                System.out.println();
            } else {
                isLooping = false;
            }
        } while (isLooping);
        return result;
    }

    public static String validateServiceChoiceId(String question, String errorMessage, List<Service> services,
            List<Service> servChoice) {
        Scanner input = new Scanner(System.in);
        String result = "";
        boolean isLooping = true;
        do {
            isLooping = true;
            PrintService.showAllService("All Service", services);
            System.out.print(question);
            result = input.nextLine();// Hendr4
            List<Service> target = new ArrayList<Service>();
            // validasi menggunakan matches
            for (Service service : services) {
                if (service.getServiceId().equalsIgnoreCase(result)) {
                    target.add(service);
                }
            }

            if (target.isEmpty()) {
                System.out.println();
                System.out.println("=============================================");
                System.out.println(errorMessage);
                System.out.println("=============================================");
                System.out.println();
            } else {
                for (Service service : servChoice) {
                    if (service.getServiceId().equalsIgnoreCase(target.get(0).getServiceId())) {
                        System.out.println();
                        System.out.println("=================================================");
                        System.out.println("ID Service already exists! Choose another service");
                        System.out.println("=================================================");
                        System.out.println();
                        target.remove(service);
                    } else {
                        isLooping = false;
                    }
                }
            }
        } while (isLooping);

        return result;
    }

    public static int numberValidation(String question, String errorMessage) {
        Scanner input = new Scanner(System.in);
        String inputAngka, regexAngka = "^[0-9]+$";

        boolean isLooping = true;
        do {
            isLooping = true;
            System.out.println(question);
            inputAngka = input.nextLine();
            // input.close();
            if (inputAngka.matches(regexAngka)) {
                isLooping = false;
            } else {
                System.out.println();
                System.out.println("=============================================");
                System.out.println(errorMessage);
                System.out.println("=============================================");
                System.out.println();
            }

        } while (isLooping);
        // input.close();

        return Integer.parseInt(inputAngka);
    }
}
