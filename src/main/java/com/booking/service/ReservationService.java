package com.booking.service;

import java.util.ArrayList;
import java.util.List;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class ReservationService {
    public static void updateReservation(List<Reservation> reservations) {
        String inputIdReservation = ValidationService.validateReservationId("Input Reservation ID : ",
                "Reservation ID Not Found ..!", reservations);
        String inputUpdateReservation = ValidationService.validateInputUpdateReservation(
                "Please Input Update Reservation Workstage : (Cancel | Finish | In Process)",
                "Please input Cancel | Finish | In Process only ..!", "Cancel|Finish|In In Process");
        for (Reservation reservation : reservations) {
            if (reservation.getReservationId().equalsIgnoreCase(inputIdReservation)) {
                reservation.setWorkstage(inputUpdateReservation);
            }
        }
        System.out.println("Update reservation successfully ..!!");
    }

    public static void createReservation(List<Person> persons, List<Service> services,
            List<Reservation> reservationList) {
        String inputCustomerId, inputEmpId, inputServiceId;
        Customer customer;
        Employee employee;
        Service service;

        inputCustomerId = ValidationService.validateCustomerId("Please input Customer ID : ", "Customer Not Found ..!",
                persons);
        inputEmpId = ValidationService.validateEmpId("Please input Employee ID : ", "Employee Not Found ..!", persons);
        customer = getCustomerByCustomerId(inputCustomerId, persons);
        employee = getEmployeeByEmployeeId(inputEmpId, persons);
        String reserveId = generateReservationID(reservationList);
        List<Service> servicesChoice = new ArrayList<Service>();

        inputServiceId = ValidationService.validateServiceId("Please input Service ID : ", "Service Not Found ..!",
                services);
        service = getServiceByServiceId(inputServiceId, services);
        servicesChoice.add(service);

        int isChoice;
        do {
            isChoice = ValidationService.numberValidation(
                    "Do you wanto to try another service ? (press (0) for No and press (1) for Yes)",
                    "Please input number only ..! (press (0) for No and press (1) for Yes)");
            switch (isChoice) {
                case 0:
                    System.out.println();
                    System.out.println("<-----");
                    System.out.println("Back to Main Menu !");
                    System.out.println("<-----");
                    System.out.println();
                    break;
                case 1:
                    inputServiceId = ValidationService.validateServiceChoiceId("Please input Service ID : ",
                            "Service Not Found ..!",
                            services, servicesChoice);
                    service = getServiceByServiceId(inputServiceId, services);
                    servicesChoice.add(service);
                default:
                    break;
            }

        } while (isChoice != 0);
        double totalService = 0.0;
        for (Service sc : servicesChoice) {
            totalService += sc.getPrice();
        }
        if ((customer.getMoney() - totalService) < 0.0) {
            System.out.println();
            System.out.println("Reservation Fail, Customer Balance is Insufficient");
            System.out.println();
        } else {
            Reservation reservation = new Reservation(reserveId, customer, employee,
                    servicesChoice, "Waiting");
            reservationList.add(reservation);
            customer.setMoney(customer.getMoney() - totalService);
            System.out.println();
            System.out.println("Reservation Successfully Added !!");
            System.out.println("Customer ID "+ customer.getId() + "Balance Now is " + String.format("%s%,d.00", "Rp. ", (int) customer.getMoney()));
            System.out.println();
        }
    }

    public static Customer getCustomerByCustomerId(String id, List<Person> customers) {
        Customer customer = new Customer();
        for (Person person : customers) {
            if (person instanceof Customer) {
                if (((Customer) person).getId().equals(id)) {
                    customer = ((Customer) person);
                }
            }
        }
        return customer;
    }

    public static String generateReservationID(List<Reservation> reservationList) {
        String getReservationID = "";
        int firstNumber = 1;
        if (reservationList.size() == 0) {
            getReservationID = "Rsv-00" + firstNumber;
        } else {
            int tempNumber = reservationList.size() + 1;
            if (tempNumber % 10 == 0) {
                getReservationID = "Rsv-0" + tempNumber;
            } else {
                getReservationID = "Rsv-00" + tempNumber;
            }
        }
        return getReservationID;
    }

    public static Employee getEmployeeByEmployeeId(String id, List<Person> employees) {
        Employee employee = new Employee();
        for (Person emp : employees) {
            if (emp instanceof Employee) {
                if (((Employee) emp).getId().equals(id)) {
                    employee = ((Employee) emp);
                }
            }
        }
        return employee;
    }

    public static Service getServiceByServiceId(String id, List<Service> services) {
        Service service = new Service();
        for (Service serv : services) {
            if (serv.getServiceId().equalsIgnoreCase(id)) {
                service = serv;
            }
        }
        return service;
    }

    public static void editReservationWorkstage() {

    }

    // Silahkan tambahkan function lain, dan ubah function diatas sesuai kebutuhan
}
