package com.booking.service;

import java.util.List;

import com.booking.models.Customer;
import com.booking.models.Employee;
import com.booking.models.Person;
import com.booking.models.Reservation;
import com.booking.models.Service;

public class PrintService {

    public static void printMenu(String title, String[] menuArr) {
        int num = 1;
        System.out.println(title);
        for (int i = 0; i < menuArr.length; i++) {
            if (i == (menuArr.length - 1)) {
                num = 0;
            }
            System.out.println(num + ". " + menuArr[i]);
            num++;
        }
    }

    public static String printServices(List<Service> serviceList) {
        String result = "";
        String[] serviceName = new String[serviceList.size()];
        for (int i = 0; i < serviceList.size(); i++) {
            serviceName[i] = serviceList.get(i).getServiceName();
        }
        result = String.join(",", serviceName);
        // Bisa disesuaikan kembali
        // for (Service service : serviceList) {
        // result += service.getServiceName() + ", ";
        // }
        return result;
    }

    // Function yang dibuat hanya sebgai contoh bisa disesuaikan kembali
    public static void showRecentReservation(List<Reservation> reservationList) {
        int num = 1;
        System.out.println();
        System.out
                .println(
                        "+====================================================================================================+");
        System.out.printf("| %-4s | %-8s | %-11s | %-15s | %-15s | %-15s | %-10s |\n",
                "No.", "ID", "Nama Customer", "Service", "Biaya Service", "Pegawai", "Workstage");
        System.out
                .println(
                        "+====================================================================================================+");
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Waiting")
                    || reservation.getWorkstage().equalsIgnoreCase("In Process")) {
                System.out.printf("| %-4s | %-4s | %-11s | %-15s | %-15s | %-15s | %-10s |\n",
                        num, reservation.getReservationId(), reservation.getCustomer().getName(),
                        printServices(reservation.getServices()),
                        (String.format("%s%,d.00", "Rp. ", (int) reservation.getReservationPrice())),
                        reservation.getEmployee().getName(), reservation.getWorkstage());
                num++;
            }
        }
        System.out
                .println(
                        "+====================================================================================================+");
        System.out.println();
    }

    public static void showAllReservation(String title, List<Reservation> reservationList) {
        int num = 1;
        System.out.println(title);
        System.out
                .println("+========================================================================================+");
        System.out.printf("| %-4s | %-8s | %-30s | %-15s |\n",
                "No.", "ID", "Service", "Biaya Service");
        System.out
                .println("+========================================================================================+");
        for (Reservation reservation : reservationList) {
            if (reservation.getWorkstage().equalsIgnoreCase("Waiting")
                    || reservation.getWorkstage().equalsIgnoreCase("In process")) {
                System.out.printf("| %-4s | %-8s | %-30s | %-15s |\n",
                        num, reservation.getReservationId(),
                        printServices(reservation.getServices()),
                        (String.format("%s%,d.00", "Rp. ", (int) reservation.getReservationPrice())));
                num++;
            }
        }
        System.out
                .println("+========================================================================================+");
    }

    public static void showAllCustomer(String title, List<Person> customers) {
        int num = 1;
        System.out.println();
        System.out.println(title);
        String formatTabel = "| %-4s | %-8s | %-11s | %-15s | %-12s | %-15s | %n";
        System.out
                .println("+==================================================================================+");
        System.out.printf(formatTabel, "No.", "ID", "Nama", "Alamat", "Membership", "Uang");
        System.out
                .println("+==================================================================================+");
        for (Person customer : customers) {
            if (customer instanceof Customer) {
                System.out.printf(formatTabel, num, customer.getId(), customer.getName(), customer.getAddress(),
                        ((Customer) customer).getMember().getMembershipName(),
                        (String.format("%s%,d.00", "Rp. ", (int) ((Customer) customer).getMoney())));
            }
            num++;
        }
        System.out
                .println("+==================================================================================+");
        System.out.println();
    }

    public static void showAllEmployee(String title, List<Person> employees) {
        int num = 1;
        System.out.println();
        System.out.println(title);
        String formatTabel = "| %-4s | %-8s | %-11s | %-15s | %-12s | %n";
        System.out
                .println("+================================================================+");
        System.out.printf(formatTabel, "No.", "ID", "Nama", "Alamat", "Experience");
        System.out
                .println("+================================================================+");
        for (Person employee : employees) {
            if (employee instanceof Employee) {
                System.out.printf(formatTabel, num, employee.getId(), employee.getName(), employee.getAddress(),
                        ((Employee) employee).getExperience());
                num++;
            }
        }
        System.out
                .println("+================================================================+");
        System.out.println();
    }

    public static void showAllService(String title, List<Service> services) {
        int num = 1;
        System.out.println(title);
        String formatTabel = "| %-4s | %-8s | %-20s | %-15s | %n";
        System.out
                .println("+========================================================================================+");
        System.out.printf(formatTabel, "No.", "ID", "Nama", "Harga");
        System.out
                .println("+========================================================================================+");
        for (Service service : services) {
            System.out.printf(formatTabel, num, service.getServiceId(), service.getServiceName(),
                    (String.format("%s%,d.00", "Rp. ", (int) service.getPrice())));
            num++;
        }
        System.out
                .println("+========================================================================================+");
    }

    public static void showHistoryReservation(String title, List<Reservation> listReservations) {
        int num = 1;
        double totalUntung = 0.0;
        System.out.println();
        System.out.println(title);
        String formatTabel = "| %-4s | %-8s | %-18s | %-20s | %-15s | %-13s | %n";
        System.out
                .println(
                        "+===============================================================================================+");
        System.out.printf(formatTabel, "No.", "ID", "Nama Customer", "Service", "Total Biaya", "Workstage");
        System.out
                .println(
                        "+===============================================================================================+");
        for (Reservation reservation : listReservations) {
            if (reservation.getWorkstage().equalsIgnoreCase("Finish")
                    || reservation.getWorkstage().equalsIgnoreCase("Cancel")) {
                System.out.printf(formatTabel, num, reservation.getReservationId(), reservation.getCustomer().getName(),
                        printServices(reservation.getServices()),
                        (String.format("%s%,d.00", "Rp. ", (int) reservation.getReservationPrice())),
                        reservation.getEmployee().getName(), reservation.getWorkstage());
            }
            if (reservation.getWorkstage().equalsIgnoreCase("Finish")) {
                totalUntung += reservation.getReservationPrice();
            }
            num++;
        }
        System.out
                .println(
                        "+===============================================================================================+");
        System.out.printf("%s%s%,d.00 %n", "Total : ", "Rp. ", (int) totalUntung);
        System.out
                .println(
                        "+===============================================================================================+");
        System.out.println();
    }
}
